\documentclass[12pt,a4paper,oneside]{article}

\usepackage{tkz-euclide}
\usepackage[paper=a4paper]{geometry}
\usepackage{polski}
\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{textcomp}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    citecolor=black,
    filecolor=black,
    urlcolor=black,
}
\pagenumbering{gobble}

\fontsize{4cm}{1em}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ScanBuffer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Przeglądanie pikseli trójkąta - Metoda 1 (ScanBuffer)}
\label{sec:drawTriangle}

Posiadając trzy punkty na ekranie \texttt{A = (ax, ay), B = (bx, by), C = (cx, cy)}
sortujemy je według wielkości y. Niech \texttt{ymin, ymid, ymax} będą posortowanymi
punktami \(A, B, C\) o rosnącej wielkości y.

Tworzymy pomocniczą tablicę \texttt{scanBuffer} o długości 2 razy większej
niż wysokość ekranu. Każdy jej wiersz przechowywać będzie dwójkę liczb - dwa
indeksy kolumn ekranu \texttt{(xmin, xmax)}.
\begin{lstlisting}[language=C]
    int scanBuffer[height * 2];
\end{lstlisting}
Para \texttt{(xmin, xmax)} oznaczać będzie to, że i-ty wiersz obrazu wypełniony
zostanie zaczynając od kolumny \texttt{xmin} do kolumny \texttt{xmax}.
Algorytm będzie polegał na przejściu każdego piksela każdej krawędzi rysowanego
trójkąta przy jednoczesnym uzupełnianiu jednej z wartości \texttt{xmin} lub \texttt{xmax}.

Do poprawnego uzupełnienia wartości tablicy potrzeba ustalić, po której stronie
prostej przechodzącej przez punkty \texttt{ymin, ymax} leży punkt \texttt{ymid}.
Można to sprawdzić obliczając moduł iloczynu wektorowego:

\begin{gather*}
    side := |(C - A) \times (B - A)| = (cx - ax)(by - ay) - (cy - ay)(bx - ax)
\end{gather*}
Jeśli wartość \texttt{side} będzie dodatnia, to znaczy że \texttt{C=(cx, cy)} leży po prawej
stronie prostej. Jeśli będzie ujemna, to znaczy że \texttt{C} leży po lewej
stronie. Wyniesie 0, jeśli wszystkie podane punkty leżą na jednej prostej.


Niech zmienna \texttt{side} oznacza położenie punktu \texttt{ymid} względem prostej
przechodzącej przez \texttt{ymin, ymax}:
\begin{lstlisting}[language=C]
    int side = pointOnSide(ymin, ymax, ymid) >= 0 ? 0 : 1;
\end{lstlisting}
Jeśli \texttt{ymid} znajdzie się po prawej stronie, to \texttt{side} przyjmie wartość
0, oraz 1 w przeciwnym wypadku.

Wiedząc, że \texttt{ymid} jest po prawej stronie prostej, tablica \texttt{scanBuffer}
uzupełniona będzie w następującej kolejności:
\begin{itemize}
    \item{Uzupełnij wartości \texttt{xmin} w \texttt{scanBuffer} od punktu \texttt{ymin} do \texttt{ymax}}
    \item{Uzupełnij wartości \texttt{xmax} w \texttt{scanBuffer} od punktu \texttt{ymin} do \texttt{ymid}}
    \item{Uzupełnij wartości \texttt{xmax} w \texttt{scanBuffer} od punktu \texttt{ymid} do \texttt{ymax}}
\end{itemize}

Jeśli zaś \texttt{ymid} znajdzie się po lewej stronie, procedura przebiegnie nieco inaczej:
\begin{itemize}
    \item{Uzupełnij wartości \texttt{xmax} w \texttt{scanBuffer} od punktu \texttt{ymin} do \texttt{ymax}}
    \item{Uzupełnij wartości \texttt{xmin} w \texttt{scanBuffer} od punktu \texttt{ymin} do \texttt{ymid}}
    \item{Uzupełnij wartości \texttt{xmin} w \texttt{scanBuffer} od punktu \texttt{ymid} do \texttt{ymax}}
\end{itemize}

Dostrzegając różnice w powyższych wywołaniach procedurę
niezależną od pozycji \texttt{side} wywołania da się zapisać tak:
\begin{lstlisting}[language=C]
    updateScanBuffer(ymin, ymax, side);
    updateScanBuffer(ymin, ymid, 1 - side);
    updateScanBuffer(ymid, ymax, 1 - side);
\end{lstlisting}

Gdzie \texttt{updateScanBuffer} będzie funkcją aktualizującą tablicę \texttt{scanBuffer}
od wiersza \texttt{a.y} do \texttt{b.y}:
\begin{lstlisting}[language=C]
void updateScanBuffer(Point a, Point b, int side) {
    float x, xstep;
    int y;

    xstep = (float)(b.x - a.x) / (float)(b.y - a.y);
    for(y = a.y, x = (float)a.x; y < b.y; y++, x += xstep) {
        scanBuffer[y * 2 + side] = (int)x;
    }
}
\end{lstlisting}
Parametr \texttt{side} wyznacza wartość aktualizowaną w tablicy, 0 oznaczać będzie
wartość minimalną \texttt{xmin}, a wartość 1 wartość maksymalną \texttt{xmax}.
Funkcja przebiega od wiersza \texttt{a.y} do wiersza \texttt{b.y}, zwiększając
wartość X o przyrost \texttt{xstep} - wielkość x przypadającą na pojedynczą iterację.
Wartości tablicy o indeksach podzielnych przez 2 wyznaczają \texttt{xmin}, 
te o niepodzielnych wyznaczają \texttt{xmax}. 


W takiej sytuacji, po uzupełnieniu tablicy \texttt{scanBuffer} trzema wywołaniami
\texttt{updateScanBuffer} jesteśmy w stanie wypełnić wszystkie piksele trójkąta
przeglądając obliczone indeksy kolumn:
\begin{lstlisting}[language=C]
    drawScanBuffer(ymin, ymax, rgb);
    drawScanBuffer(ymin, ymid, rgb);
    drawScanBuffer(ymid, ymax, rgb);
\end{lstlisting}
gdzie \texttt{drawScanBuffer} przegląda dwójki \texttt{(xmin, xmax)} w tablicy
\texttt{scanBuffer} od wiersza \texttt{a.y} do wiersza \texttt{b.y}:
\begin{lstlisting}[language=C]
void drawScanBuffer(Point a, Point b, RGB rgb) {
    for(int y = a.y; y < b.y; y++) {
        int xmin = scanBuffer[y * 2];
        int xmax = scanBuffer[y * 2 + 1];

        for(int x = xmin; x < xmax; x++)
            setPixel(x, y, rgb);
    }
}
\end{lstlisting}

\section{Przeglądanie pikseli trójkąta - Metoda 2}

Podobnie jak w poprzedniej metodzie, wymagane jest posortowanie wierzchołków
trójkąta według rosnącej wartości Y.
Niech \(A = (ax, ay), B = (bx, by), C = (cx, cy)\) 
będą posortowanymi względem Y punktami na ekranie.

Obliczamy ilość pikseli w pionie (Y) między dwoma każdymi wierzchołkami trójkąta:
\begin{lstlisting}[language=C]
    ab_ysteps = by - ay;
    bc_ysteps = cy - by;
    ac_ysteps = cy - ay;
\end{lstlisting}

Następnie liczymy przyrosty wartości X między każdym z punktów:
\begin{lstlisting}[language=C]
    dx1 = (ab_ysteps > 0) ? (bx - ax) / ab_ysteps : 0.0f;
    dx2 = (bc_ysteps > 0) ? (cx - bx) / bc_ysteps : 0.0f;
    dx3 = (ac_ysteps > 0) ? (cx - ax) / ac_ysteps : 0.0f;
\end{lstlisting}

Algorytm będzie przeglądał wszystkie poziome linie trójkąta od jego góry
(od wiersza \(ay\)) do jego dołu (do wiersza \(cy\)).
Wprowadzamy odcinek \(ES, S = (sx, sy), E = (ex, ey)\) oznaczający pojedynczą
poziomą linię łączącą dwie krawędzie trójkąta.

Korzystamy z funkcji \texttt{pointOnSide();} do zbadania położenia
punktu b względem prostej przechodzącej przez punkty a i c:
\begin{lstlisting}[language=C]
    side = pointOnSide(a, c, b);
\end{lstlisting}

\clearpage

Na początku wierzchołki E i C wychodzą od punktu A:
\begin{lstlisting}[language=C]
    (sx, sy) = (ax, ay);
    (ex, ey) = (ax, ay);
\end{lstlisting}

Poniższe 2 pętle for przesuwają odcinek SE od ay do cy. Poniższy pseudokod
dotyczy sytuacji, gdy punkt B leży po lewej stronie prostej AC (side \(<\) 0).
To oznacza, że punkt S trafi od punktu A do punktu B, a następnie od punktu B do punktu C.

\begin{center}
\includegraphics[width=120px]{tri1}
\end{center}

W obu pętlach przyrost wartości \texttt{ex} nie zmieniał się, ponieważ
punkt E dążył od punktu A do C. Wartość \texttt{sx} natomiast przybliżała
wartość x między dwoma różnymi odcinkami: AB i CB.
\begin{lstlisting}[language=C]
    for(; sy < by; sy++, sx += dx1, ex += dx3)
        drawHorizontalLine(sx, ex, sy);

    (sx, sy) = (bx, by);
    for(; sy < cy; sy++, sx += dx2, ex += dx3)
        drawHorizontalLine(sx, ex, sy);
\end{lstlisting}

\begin{center}
\includegraphics[width=120px]{tri2}
\end{center}

Jeżeli B znajdzie się po przeciwnej stronie prostej AC (side \(\geq\) 0), to
wtedy punkt S będzie dążył bezpośrednio od punktu A do C, a punkt E zachowa się
tak jak punkt S dla (side \(<\) 0).
\begin{lstlisting}[language=C]
    for(; ey < by; ey++, sx += dx3, ex += dx1)
        drawHorizontalLine(sx, ex, ey);

    (ex, ey) = (bx, by);
    for(; ey < cy; ey++, sx += dx3; ex += dx2)
        drawHorizontalLine(sx, ex, ey);
\end{lstlisting}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Interpolacja wartości w trójkącie.}
\label{sec:gradients}

\subsection{Współrzędne barycentryczne}
\label{sub:bary}

Niech P będzie pewnym punktem zapisanym jako kombinacja liniowa:
\begin{equation*}
    P = u P_0 + v P_1 + w P_2
\end{equation*} 
Wówczas punkt P należy do trójkąta \(P_0, P_1, P_2\), jeśli:
\begin{itemize}
    \item{\(u, v, w \in [0, 1]\)}
    \item{\(u + v + w = 1\)}
\end{itemize}

Do obliczenia wartości \(u, v\) wykorzystujemy fakt, że \(w = 1 - u - v\).
\begin{equation*}
    P - P_2 = u (P_0 - P_2) + v (P_1 - P_2)
\end{equation*}

Co po rozwinięciu dla każdej współrzędnej daje:
\begin{gather*}
    x - x2 = u(x0 - x2) + v(x1 - x2) \\
    y - y2 = u(y0 - y2) + v(y1 - y2)
\end{gather*}

Rozwiązujemy układ równań dla zmiennych \(u, v\) wg wzorów Cramera:
\begin{gather*}
    A = \begin{vmatrix}
        (x0 - x2) & (x1 - x2) \\
        (y0 - y2) & (y1 - y2)
    \end{vmatrix} = (x0 - x2)(y1 - y2) - (y0 - y2)(x1 - x2) \\
    A_u = \begin{vmatrix}
        (x - x2) & (x1 - x2) \\
        (y - y2) & (y1 - y2)
    \end{vmatrix} = (x - x2)(y1 - y2) - (y - y2)(x1 - x2) \\
    A_v = \begin{vmatrix}
        (x0 - x2) & (x - x2) \\
        (y0 - y2) & (y - y2)
    \end{vmatrix} = (x0 - x2)(y - y2) - (y0 - y2)(x - x2) \\
\end{gather*}

Wówczas \(u = \frac{A_u}{A}, v = \frac{A_v}{A}, w = 1 - u - w\), przy czym \(A \neq 0\).

\subsection{Obliczanie stałego gradientu dla trójkąta}
\label{sub:gradients}

Celem tego rozdziału jest pokazanie, jak obliczyć stałe przyrosty
(constant gradients) pewnej wartości (ozn. C) w pionie i w poziomie. Ma to przyspieszyć
proces rasteryzacji trójkątów - zamiast ciągłej kalkulacji wartości, wartość
dla bieżącego piksela będzie inkrementowana o stały przyrost.

Niech \(P_{0} = ymin, P_{1} = ymid, P_{2} = ymax\) będą posortowanymi według Y 
wierzchołkami trójkąta.
Każdy wierzchołek \(P_{i}\) posiada pewną dodatkową wartość z nim związaną \(c_{i}\),
może to być np. głębia położenia wierzchołka, kolor.
\begin{gather*}
    P_{i} = (x_{i}, y_{i}, c_{i}), i = 0, 1, \ldots
\end{gather*}

Dodajemy 2 pomocnicze punkty:
\begin{itemize}
    \item{\(P_3 := (x_3, y_3, c_3)\) taki, że \(y_3 = y_1\).}
    \item{\(P_4 := (x_4, y_4, c_4)\) taki, że \(x_4 = x_2\).}
\end{itemize}

Poszukujemy przyrostów wartości C w pionie i w poziomie, stałych dla całego trójkąta:
\begin{gather*}
    \frac{dC}{dx} = \frac{c_1 - c_3}{x_1 - x_3} \\
    \frac{dC}{dy} = \frac{c_2 - c_4}{y_2 - y_4}
\end{gather*}

Powinniśmy więc znaleźć wartości \(c_4, c_3, y_4, x_3\) zależne od pozostałych
punktów trójkąta. Każdą z nich zapisujemy korzystając z interpolacji liniowej:
\begin{equation*}
    X = A + t (B - A), t \in [0, 1]
\end{equation*}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.2\linewidth]{triangleInterp2}
    \caption{Ilustracja położenia punktów \(P_3, P_4\)}
    \label{fig:triangleInterp2}
\end{figure}

Wartość \(x_3\) to interpolacja wartości X w przedziale \([x_0, x_2]\):
\begin{equation*}
    x_3 = x_0 + \frac{(y_3 - y_0)}{(y_2 - y_0)} (x_2 - x_0) =
          x_0 + \frac{(y_1 - y_0)}{(y_2 - y_0)} (x_2 - x_0)
\end{equation*}

Wartość \(c_3\) można zapisać jako pośrednią między \(c_0\) i \(c_2\):
\begin{equation*}
    c_3 = c_0 + \frac{(y_3 - y_0)}{(y_2 - y_0)} (c_2 - c_0) = 
          c_0 + \frac{(y_1 - y_0)}{(y_2 - y_0)} (c_2 - c_0)
\end{equation*}

Wartość \(y_4\) to interpolacja wartości Y w przedziale \([y_0, y_1]\):
\begin{equation*}
    y_4 = y_0 + \frac{(x_4 - x_0)}{(x_1 - x_0)} (y_1 - y_0) = 
          y_0 + \frac{(x_2 - x_0)}{(x_1 - x_0)} (y_1 - y_0)
\end{equation*}

Wartość \(c_4\) to interpolacja wartości C w przedziale \([c_0, c_1]\):
\begin{equation*}
    c_4 = c_0 + \frac{(x_4 - x_0)}{(x_1 - x_0)} (c_1 - c_0) =
          c_0 + \frac{(x_2 - x_0)}{(x_1 - x_0)} (c_1 - c_0)
\end{equation*}

\begin{gather*}
    \frac{dC}{dx} = \frac{c_1 - c_3}{x_1 - x_3} =
    \frac
    {(c_1 - c_0) - \frac{(y_1 - y_0)}{(y_2 - y_0)} (c_2 - c_0)}
    {(x_1 - x_0) - \frac{(y_1 - y_0)}{(y_2 - y_0)} (x_2 - x_0)} =
    \frac
    {(c_1 - c_0) - \frac{(y_1 - y_0)}{(y_2 - y_0)} (c_2 - c_0)}
    {(x_1 - x_0) - \frac{(y_1 - y_0)}{(y_2 - y_0)} (x_2 - x_0)}
    \frac{\frac{(y_2 - y_0)}{(y_2 - y_0)}}{\frac{(y_2 - y_0)}{(y_2 - y_0)}}
\end{gather*}

\begin{gather*}
    \frac{dC}{dy} = \frac{c_2 - c_4}{y_2 - y_4} = 
    \frac
    {(c_2 - c_0) - \frac{(x_2 - x_0)}{(x_1 - x_0)} (c_1 - c_0)}
    {(y_2 - y_0) - \frac{(x_2 - x_0)}{(x_1 - x_0)} (y_1 - y_0)} =
    \frac
    {(c_2 - c_0) - \frac{(x_2 - x_0)}{(x_1 - x_0)} (c_1 - c_0)}
    {(y_2 - y_0) - \frac{(x_2 - x_0)}{(x_1 - x_0)} (y_1 - y_0)}
    \frac{ \frac{(x_2 - x_0)}{(x_2 - x_0)} }{ \frac{(x_2 - x_0)}{(x_2 - x_0)} }
\end{gather*}

\begin{gather*}
    \frac{dC}{dx} = 
    \frac
    {(c_1 - c_0)(y_2 - y_0) - (y_1 - y_0)(c_2 - c_0)} 
    {(x_1 - x_0)(y_2 - y_0) - (y_1 - y_0)(x_2 - x_0)}  \\
    \frac{dC}{dy} = 
    \frac
    {(c_2 - c_0)(x_1 - x_0) - (x_2 - x_0)(c_1 - c_0)}
    {(y_2 - y_0)(x_1 - x_0) - (x_2 - x_0)(y_1 - y_0)}
\end{gather*}

Korzystając z współrzędnych barycentrycznych, wartość dla piksela \((x, y)\)
obliczyć jest bardzo łatwo:
\begin{gather*}
    C(x, y) = u c_0 + v c_1 + w c_2 \\
    C(x_i, y_i) = c_i, i = 0, 1, 2
\end{gather*}

Dla obliczonych stałych przyrostów funkcja będzie działać następująco:
\begin{equation*}
    C(x, y) = c_0 + y \frac{dC}{dy} + (x  - min\{x0, x1, x2\}) \frac{dC}{dx}, x, y = 0, 1, 2, \ldots
\end{equation*}

\end{document}
% vim: set syntax=plaintex:
