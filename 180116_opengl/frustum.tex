\documentclass[12pt,a4paper,oneside]{article}

\usepackage[paper=a4paper]{geometry}
\usepackage{polski}
\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{textcomp}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    citecolor=black,
    filecolor=black,
    urlcolor=black,
}
\pagenumbering{gobble}

\fontsize{4cm}{1em}

\begin{document}

\section{Defining viewing frustum}
\label{sec:clipping}

\begin{itemize}
    \item{Camera is looking at negative Z axis}
    \item{ l, r - left, right X clipping planes}
    \item{ b, t - bottom, top Y clipping planes}
    \item{ n, f - near, far Z clipping planes (positive values!)}
    \item{\( (x_e, y_e, z_e) \) - coordinates in eye space (input vector in eye coords)}
    \item{\( (x_p, y_p) \) - screen coordinates of initial point}
    \item{\( (x_c, y_c, z_c, w_c) \) - coordinates in clipping space (clip coords)
        \begin{gather*}
            ( -w_c <= x_c, y_c, z_c <= w_c ) \Longleftrightarrow ( \begin{bmatrix}
                x_c \\
                y_c \\
                z_c \\
                w_c
            \end{bmatrix} \in VF )
        \end{gather*}
    }
    \item{\( (x_n, y_n, z_n) \) - normalized coordinates (clip coords divided by \(w_c\)).
        \begin{gather*}
            ( -1 <= x_n, y_n, z_n <= 1 ) \Longleftrightarrow ( \begin{bmatrix}
                x_n \\
                y_n \\
                z_n
            \end{bmatrix} \in VF )\\
            (x_n, y_n, z_n) = (\frac{x_c}{w_c}, \frac{y_c}{w_c}, \frac{z_c}{w_c})
        \end{gather*}
    }
    \item{
        We're going to find matrix V that transforms 3-dimensional vector
        in eye space into 4-dimensional homogenous vector in clipping space. 
        \begin{gather*}
            V 
            \begin{bmatrix}
                x_e \\
                y_e \\
                z_e \\
                w_e = 1
            \end{bmatrix}
             = 
            \begin{bmatrix}
                A_1     & 0     &   B_1 & 0     \\
                0       & A_2   &   B_2 & 0     \\
                0       & 0     &   A_3 & B_3   \\
                0       & 0     &    -1 & 0
            \end{bmatrix}
            \begin{bmatrix}
                x_e \\
                y_e \\
                z_e \\
                1
            \end{bmatrix}
            =
            \begin{bmatrix}
                A_1 x_e + B_1 z_e \\
                A_2 y_e + B_2 z_e \\
                A_3 z_e + B_3 \\
                -z_e
            \end{bmatrix}
            =
            \begin{bmatrix}
                x_c \\
                y_c \\
                z_c \\
                w_c
            \end{bmatrix}
        \end{gather*}

        % After normalization of homogenous vector (i.e. before perspective division)
        \begin{gather*}
            \begin{bmatrix}
                \frac{x_c}{w_c} \\
                \frac{y_c}{w_c} \\
                \frac{z_c}{w_c} 
            \end{bmatrix}
            =
            \begin{bmatrix}
                (A_1 x_e + B_1 z_e) / ( -z_e ) \\
                (A_2 y_e + B_2 z_e) / ( -z_e ) \\
                (A_3 z_e + B_3) / ( -z_e ) \\
            \end{bmatrix}
        \end{gather*}
    }
\end{itemize}

\clearpage

\subsection{Mapping X, Y to NDC}
\label{sub:mapping_xy_to_ndc}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% X -> NDC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{ Find \(x\) position of point on near plane: }

Using similar triangles ratio:
\begin{gather*}
    \frac{ x_p }{ n } = \frac{ x_e }{ -z_e } \implies
    \boxed{
    x_p = n \frac{ x_e }{ -z_e }
    }
\end{gather*}

\begin{center}
    \includegraphics[width=200px]{frustum1.png}
\end{center}

\subsubsection{ Linear mapping \(x_p \in [l, r] \to x_n \in [-1, 1]\): }

\begin{center}
    \includegraphics[width=200px]{frustum3.png}
\end{center}

1. Find slope:
\begin{gather*}
    A = \frac{1 - (-1)}{r - l} = \frac{2}{r - l}
\end{gather*}

2. \(l\) is mapped to -1, and \(r\) is mapped to 1, thus we can insert points \((l, -1)\)
and \((r, 1)\) into line equations:
\begin{gather*}
    A l + B = -1 \\
    A r + B =  1 \\
    \implies \\
    B = 1 - Ar = 1 - \frac{2r}{r - l} = \frac{r - l - 2r}{r - l} = -\frac{r + l}{r - l}
\end{gather*}

\begin{gather*}
    x_n 
    = \frac{2}{r - l} x_p - \frac{r + l}{r - l} =
    \boxed{
        \frac{2 n x_e }{(r - l) ( -z_e )} - \frac{r + l}{r - l}
    }
\end{gather*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Y -> NDC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{ Find \(y\) position of point on near plane: }

\begin{gather*}
    \frac{ y_p }{ n } = \frac{ y_e }{ -z_e } \implies
    \boxed{
    y_p = n \frac{ y_e }{ -z_e }
    }
\end{gather*}

\begin{center}
    \includegraphics[width=200px]{frustum2.png}
\end{center}

\subsubsection{ Linear mapping \(y_p \in [b, t] \to y_n \in [-1, 1]\):}

\begin{center}
    \includegraphics[width=200px]{frustum4.png}
\end{center}

\begin{gather*}
    A = \frac{1 - (-1)}{t - b} = \frac{2}{t - b}
\end{gather*}

\begin{gather*}
    A b + B = -1 \\
    A t + B =  1 \\
    \implies \\
    B = 1 - At = 1 - \frac{2b}{t - b} = \frac{t - b - 2t}{t - b} = -\frac{t + b}{t - b}
\end{gather*}

\begin{gather*}
    y_n = \frac{2}{t - b} y_p - \frac{t + b}{t - b} =
    \boxed{
        \frac{ 2 n y_e }{(t - b) ( -z_e )} - \frac{t + b}{t - b}
    }
\end{gather*}

\subsection{Mapping Z to NDC}
\label{sub:mapping_z_to_ndc}

Z mapping requires to use different approach. We cannot repeat previously mentioned steps from X, Y mapping because in persperctive projection
relation between initial Z coord and final on-screen position is not linear.
Applying linear mapping for Z coord will produce ortographic projection of input points.

Using the fact, that Z clip coord \(z_c\) after division by \(w_c\) should be normalized
\begin{gather*}
    \frac{z_c}{w_c} = z_n
\end{gather*}

we can write initial equations for \(z_n\).
\(z_n\) depends on \(z_e\) and \(z_e = (-n) \) corresponds to -1 and \(z_e = (-f)\) to 1.
\begin{gather*}
    \frac{ A z_e + B } { -z_e } = z_n \Big| (z_e := (-n) \vee z_e := (-f) \\
    \frac{ -A n + B }{ -(-n) } = -1 \implies -An + B = -n\\
    \frac{ -A f + B }{ -(-f) } = 1 \implies -Af + B = f
\end{gather*}

Next step is to solve system of 2 equations:
\begin{gather*}
    \begin{bmatrix}
        -n & 1 \\
        -f & 1
    \end{bmatrix}
    \begin{bmatrix}
        A \\
        B
    \end{bmatrix}
    =
    \begin{bmatrix}
        -n \\
        f
    \end{bmatrix}
\end{gather*}

\begin{gather*}
    Av = \begin{vmatrix}
        -n & 1 \\
        -f & 1
    \end{vmatrix} = f - n, (\forall f \ne n) \\
    A = \frac{ 
        \begin{vmatrix}
            -n & 1 \\
             f & 1
        \end{vmatrix}
    }{ Av }  = \frac{ -(f + n) }{f - n} \\
    B = \frac {
        \begin{vmatrix}
            -n & -n \\
            -f & f
        \end{vmatrix}
    }{ Av } = \frac{ -2nf }{f - n}
\end{gather*}

\begin{gather*}
    \boxed{
        z_n = \frac{ \frac{-(f + n)}{f - n}z_e + \frac{ -2nf }{f - n} }{ -z_e }
    }
\end{gather*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Projection matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Complete projection matrix}
\label{sub:building_projection_matrix}

\subsubsection{General matrix}
\begin{gather*}
    \begin{bmatrix}
        \frac{ 2n }{(r - l)} & 0 & \frac{r + l}{r - l} & 0 \\
        0 & \frac{ 2n }{(t - b)} & \frac{t + b}{t - b} & 0 \\
        0 & 0 & \frac{-(f + n)}{f - n} & \frac{ -2nf }{f - n} \\
        0 & 0 & -1 & 0
    \end{bmatrix}
\end{gather*}

\subsubsection{Simplified matrix for symmetrical frustum (r = -l, t = -b)}
\begin{gather*}
    \begin{bmatrix}
        \frac{ n }{ r } & 0 & 0 & 0 \\
        0 & \frac{ n }{ t } & 0 & 0 \\
        0 & 0 & \frac{-(f + n)}{f - n} & \frac{ -2nf }{f - n} \\
        0 & 0 & -1 & 0
    \end{bmatrix}
\end{gather*}

Values of XY clipping planes can be calculated using field of view angle:
\begin{gather*}
    hfov, vfov > 0 \\
    \tan \frac{hfov}{2} = \frac{r}{n} \implies r = n \tan \frac{hfov}{2} \\
    \tan \frac{vfov}{2} = \frac{t}{n} \implies t = n \tan \frac{vfov}{2}
\end{gather*}

or screen aspect ratio (\(\frac{width}{height}\)) and y-fov:
\begin{gather*}
    t = n \tan (\frac{vfov}{2}) \\
    r = t \cdot aspect
\end{gather*}

\subsection{Checking forumulas by substitution}
\subsubsection{ Transforming point \((l, b, -n)\): }

\begin{gather*}
    \begin{bmatrix}
        \frac{ 2n }{(r - l)} & 0 & \frac{r + l}{r - l} & 0 \\
        0 & \frac{ 2n }{(t - b)} & \frac{t + b}{t - b} & 0 \\
        0 & 0 & \frac{-(f + n)}{f - n} & \frac{ -2nf }{f - n} \\
        0 & 0 & -1 & 0
    \end{bmatrix}
    \begin{bmatrix}
        l \\
        b \\
        -n \\
        1
    \end{bmatrix}
    =
    \begin{bmatrix}
        \frac{ 2nl }{(r - l)} - \frac{n(r + l)}{r - l} = \frac{2nl - nr - nl}{r - l}\\
        \frac{ 2nb }{(t - b)} - \frac{n(t + b)}{t - b} = \frac{2nb - nt - nb}{t - n}\\
        \frac{n(f + n)}{f - n} + \frac{ -2nf }{f - n} = \frac{nf + n^{2} - 2nf}{f - n} \\
        n
    \end{bmatrix}
    =
    \begin{bmatrix}
        -n \\
        -n \\
        \frac{n(n - f)}{f - n} \\
        n
    \end{bmatrix}
    =
    \begin{bmatrix}
        -n \\
        -n \\
        -n \\
        n
    \end{bmatrix} \\
    \begin{bmatrix}
        -n / n \\
        -n / n \\
        -n / n
    \end{bmatrix}
    =
    \begin{bmatrix}
        -1 \\
        -1 \\
        -1
    \end{bmatrix}
\end{gather*}

\subsubsection{ Transforming point \((r, t, -f)\): }
\begin{gather*}
    \begin{bmatrix}
        \frac{ 2n }{(r - l)} & 0 & \frac{r + l}{r - l} & 0 \\
        0 & \frac{ 2n }{(t - b)} & \frac{t + b}{t - b} & 0 \\
        0 & 0 & \frac{-(f + n)}{f - n} & \frac{ -2nf }{f - n} \\
        0 & 0 & -1 & 0
    \end{bmatrix}
    \begin{bmatrix}
        r \\
        t \\
        -f \\
        1
    \end{bmatrix}
    =
    \begin{bmatrix}
        \frac{ 2nr }{(r - l)} - \frac{n(r + l)}{r - l} = \frac{2nr - nr - nl}{r - l}\\
        \frac{ 2nt }{(t - b)} - \frac{n(t + b)}{t - b} = \frac{2nt - nt - nb}{t - n}\\
        \frac{f(f + n)}{f - n} + \frac{ -2nf }{f - n} = \frac{nf + f^{2} - 2nf}{f - n} \\
        n
    \end{bmatrix}
    =
    \begin{bmatrix}
        n \\
        n \\
        n \\
        n
    \end{bmatrix} \\
    \begin{bmatrix}
        n / n \\
        n / n \\
        n / n \\
    \end{bmatrix}
    =
    \begin{bmatrix}
        1 \\
        1 \\
        1
    \end{bmatrix}
\end{gather*}

\end{document}
% vim: set syntax=plaintex:
